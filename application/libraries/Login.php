<?php 

class Login 
{
	private $nombre;
	private $usuario;
	private $contrasenia;


	public function getNom(){
		return $this->nombre;	
	}

	public function setNom($nom){
		$this->nombre = $nom;
	}

	public function getUsu(){
		return $this->usuario;	
	}

	public function setUsu($usu){
		$this->usuario = $usu;
	}

	public function getCont(){
		return $this->contrasenia;	
	}

	public function setCont($cla){
		$this->contrasenia = $cla;
	}
}
