-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-01-2020 a las 23:14:57
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `equipo_15`
--
CREATE DATABASE IF NOT EXISTS `equipo_15` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `equipo_15`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencia`
--

DROP TABLE IF EXISTS `competencia`;
CREATE TABLE `competencia` (
  `id_competencia` int(11) NOT NULL,
  `nombre_comp` varchar(50) NOT NULL,
  `descripcion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_competencia`
--

DROP TABLE IF EXISTS `usuario_competencia`;
CREATE TABLE `usuario_competencia` (
  `id_usuario` int(11) NOT NULL,
  `id_competancia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ususario`
--

DROP TABLE IF EXISTS `ususario`;
CREATE TABLE `ususario` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `contrasenia` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD PRIMARY KEY (`id_competencia`);

--
-- Indices de la tabla `usuario_competencia`
--
ALTER TABLE `usuario_competencia`
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_competancia` (`id_competancia`);

--
-- Indices de la tabla `ususario`
--
ALTER TABLE `ususario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencia`
--
ALTER TABLE `competencia`
  MODIFY `id_competencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ususario`
--
ALTER TABLE `ususario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario_competencia`
--
ALTER TABLE `usuario_competencia`
  ADD CONSTRAINT `usuario_competencia_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `ususario` (`id_usuario`),
  ADD CONSTRAINT `usuario_competencia_ibfk_2` FOREIGN KEY (`id_competancia`) REFERENCES `competencia` (`id_competencia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
